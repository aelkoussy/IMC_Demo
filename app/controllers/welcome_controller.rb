class WelcomeController < ApplicationController

  def index
        # data shall be retrieved from API, here we are making a demo with the static data we have
        @booking_time = [
            ['10am', 5],
            ['2pm', 36],
            ['6pm', 19],
            ['10pm', 8],
            ['2 am', 1],
            ['6 am', 1]
            ]
    @freight_capacity_data = [
        ['LCL (Less-than-container load)', 198],['20\' Container', 36],
        ['40\' Container', 19],['45\' High cube', 8]
        ]
    @freight_value_data = [
        ['LCL (Less-than-container load)', 134965.03],
        ['20\' Container', 50785.30],['40\' Container', 28520.86],
        ['45\' High cube', 14118.45]
        ]

    @ocean_value_data = [
        ['LCL (Less-than-container load)', 55643.16],
        ['20\' Container', 36452.19],['40\' Container', 16484.89],
        ['45\' High cube', 1821.75]
    ]

    @rail_value_data = [
        ['20\' Container', 14333.11],['40\' Container', 12035.97],
        ['45\' High cube', 12296.70]
    ]


    @ocean_shipments_data = [
        ['20\' Container', 31],['40\' Container', 14],
        ['45\' High cube', 1]
    ]

    @rail_shipments_data = [
        ['20\' Container', 12],['40\' Container', 7],
        ['45\' High cube', 8]
    ]
  end
end
